class VPPage {

    visit() {

        cy.visitUrlWithQueryParam('/admin/forms/loadForm/153', '?openInTab=current')

    }

    visitReport() {

        cy.visit("/admin/reports/runReport/33")
        cy.wait(8000)

    }

    verifyVendorBillSupplierAssertion(Supplier) {

        return cy.getSupplierAssertion('#dropdownlistContentbill_defs_supplier_id > .jqx-combobox-input', Supplier, 'VendorBillPayment Supplier:')
 
     }

    verifyPayTo() {

        cy.comboBoxRemoteStrict('#dropdownlistContentbill_defs_supplier_id > .jqx-combobox-input', 'V-0001 - Supplier 1')

    }

    verifyPayToAssertion() {

        cy.valueAreaInput('#dropdownlistContentbill_defs_supplier_id > .jqx-combobox-input', 'V-0001 - Supplier 1')

    }

    verifyPayFrom() {

        cy.comboBoxRemoteStrict('#dropdownlistContentbill_defs_finaccounts_id > .jqx-combobox-input', '1112')

    }

    verifyTotal() {

        cy.numberInput('#bill_defs_total', '11.88')

    }

    verifyGridCellTotal() {

        cy.getTotalGridCellValue('VendorBillPayment Total:')

    }


}

export default new VPPage();