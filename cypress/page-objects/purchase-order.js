
class POPage {

    visit() {

        cy.visitUrlWithQueryParam('/admin/forms/loadForm/139', '?openInTab=current')

    }

    visitReport() {

        cy.visit("/admin/reports/runReport/19")
        cy.wait(8000)

    }

    verifyCopyPurchaseNA() {

        cy.listbox('#dropdownlistContentpurchase_order_defs_purchaseorderdefs_id > .jqx-combobox-input')

    }

    verifyCopyPurchase() {

        cy.comboBoxRemoteStrict('#dropdownlistContentpurchase_order_defs_purchaseorderdefs_id > .jqx-combobox-input', 'PO-000400')

    }

    verifyCopyPurchaseText() {

        cy.textAreaInput('#purchase_order_defs_purchaseorderdefs_id', 'PO-000294')

    }

    verifyVersion() {

        cy.valueAreaInput('#purchase_order_defs_version > .jqx-input-content', '3')

    }

    verifyOpenOrClosePo() {

        cy.cssElement('#OPEN_OR_CLOSE_PO')

    }

    verifyStatusValue() {

        cy.valueAreaInput('#dropdownlistContentpurchase_order_defs_status > .jqx-combobox-input', 'OPEN')

    }

    verifyContains() {

        cy.contains('Data deleted successfully!')

    }

    verifyAlertAdd() {

        cy.contains("Can't be used during Add!").should('be.visible')

    }

    verifyPurchaseRequestNA() {

        cy.input('#purchaseorder_purchaseintent_purchaseintentdefs_id')

    }

    verifySinglePurchaseRequest() {

        cy.comboBoxMultiSelectArray('#purchaseorder_purchaseintent_purchaseintentdefs_id', ['PRI-00494'])

    }

    verifyMultiPurchaseRequest() {

        cy.comboBoxMultiSelectArray('#purchaseorder_purchaseintent_purchaseintentdefs_id', ['PRI-00450', 'PRI-00458'])

    }

    verifyPurchaseRequestDownenter3rd() {

        cy.downArrow3rd('#dropdownlistArrowpurchaseorder_purchaseintent_purchaseintentdefs_id > .jqx-icon-arrow-down')

    }

    verifyPurchaseRequestAssertion() {

        cy.displayedText('#dropdownlistContentpurchaseorder_purchaseintent_purchaseintentdefs_id', 'Displayed value in PurchaseOrder MultiPurchaseRequest Field:')

    }

    verifyCurrentStatusNA() {

        cy.listbox('#dropdownlistContentpurchase_order_defs_current_status > .jqx-combobox-input')

    }

    verifyCurrentStatus() {

        cy.comboBoxRemoteStrict('#dropdownlistContentpurchase_order_defs_current_status > .jqx-combobox-input', 'CUSTOM CLEARANCE')

    }

    verifyRequestedByNA() {

        cy.listbox('#dropdownlistContentpurchase_order_defs_employeemaster_id > .jqx-combobox-input')

    }

    verifyRequestedBy() {

        cy.comboBoxRemoteStrict('#dropdownlistContentpurchase_order_defs_employeemaster_id > .jqx-combobox-input', 'SIRAJUDHEEN')

    }

    verifyRequestedByAssertion() {

        cy.valueAreaInput('#dropdownlistContentpurchase_order_defs_employeemaster_id > .jqx-combobox-input', 'LENIN BONNAGEFA - 054')

    }

    verifyPurchaseOrderRequestedByAssertion(RequestedBy) {

        return cy.getRequestedByAssertion('#dropdownlistContentpurchase_order_defs_employeemaster_id > .jqx-combobox-input', RequestedBy, 'PurchaseOrder RequestedBy:')

    }

    verifyGridCellValueItemName() {

        return cy.getGridCellValue('purchase_order_items', 'purchase_order_items_itemmaster_idDisplay');

    }

    verifyGridCellValueQty() {

        return cy.getGridCellValue('purchase_order_items', 'purchase_order_items_qty');

    }

    verifyGridCellValueItemNameAssertion(ItemName) {

        return cy.getGridCellValueAssertion('purchase_order_items', 'purchase_order_items_itemmaster_idDisplay', ItemName, 'PurchaseOrder ItemName:')

    }

    verifyGridCellValueQtyAssertion(Qty) {

        return cy.getGridCellValueAssertion('purchase_order_items', 'purchase_order_items_qty', Qty, 'PurchaseOrder Qty:')

    }

    verifyNotes() {

        cy.comboBoxStrict('#purchase_order_defs_memo', 'purchase order')

    }

    verifyNotesValue() {

        cy.valueAreaInput('#purchase_order_defs_memo', 'purchase order')

    }

    verifySupplier() {

        cy.comboBoxRemoteStrict('#dropdownlistContentpurchase_order_defs_supplier_id > .jqx-combobox-input', 'V-0001 - Supplier 1')

    }

    verifyPurchaseOrderSupplier() {

        return cy.getStoreVariable('#dropdownlistContentpurchase_order_defs_supplier_id > .jqx-combobox-input')

    }

    verifySupplierValue() {

        cy.valueAreaInput('#dropdownlistContentpurchase_order_defs_supplier_id > .jqx-combobox-input', 'V-0001 - Supplier 1')

    }

    verifySupplierMandatoryFields() {

        cy.textAreaInput('#purchase_order_defs_supplier_id_div > .formInputFields > .jqx-validator-error-label', 'SUPPLIER is required')

    }

    verifyAttn() {

        cy.comboBoxStrict('#purchase_order_defs_attn', 'Supplier')

    }

    verifyAttnValue() {

        cy.valueAreaInput('#purchase_order_defs_attn', 'Person 1')

    }

    verifyPaymentMethod() {

        cy.comboBoxRemoteStrict('#dropdownlistContentpurchase_order_defs_valpaymentmethods_id > .jqx-combobox-input', 'Cash')

    }

    verifyCurrentPoDate() {

        cy.dateTimeInput('#purchase_order_defs_date', '13/11/2022')

    }

    verifyPastPoDate() {

        cy.dateTimeInput('#purchase_order_defs_date', '26/10/2021')

    }

    verifyFuturePoDate() {

        cy.dateTimeInput('#purchase_order_defs_date', '12/12/2022')

    }

    verifyPoDateValue() {

        cy.valueAreaInput('#purchase_order_defs_date', '26/10/2021')

    }

    verifyPaymentType() {

        cy.comboBoxRemoteStrict('#dropdownlistContentpurchase_order_defs_type > .jqx-combobox-input', 'BANK TRANSFER')

    }

    verifyPaymentTypeValue() {

        cy.valueAreaInput('#dropdownlistContentpurchase_order_defs_type > .jqx-combobox-input', 'CASH')

    }

    verifyPurchaseOrderCustomerJob() {

        return cy.getStoreVariable('#dropdownlistContentpurchase_order_defs_customer_id > .jqx-combobox-input')

    }

    verifyPurchaseOrderCustomerJobAssertion(CustomerJob) {

        return cy.getCustomerJobAssertion('#dropdownlistContentpurchase_order_defs_customer_id > .jqx-combobox-input', CustomerJob, 'PurchaseOrder CustomerJob:')

    }

    verifyCustomerJob() {

        cy.comboBoxRemoteStrict('#dropdownlistContentpurchase_order_defs_customer_id > .jqx-combobox-input', 'C-1503')

    }

    verifyCustomerJobAssertion() {

        cy.valueAreaInput('#dropdownlistContentpurchase_order_defs_customer_id > .jqx-combobox-input', 'C-1503 - QA Sales receipts')

    }

    verifyFinancialCentre() {

        cy.comboBoxRemoteStrict('#dropdownlistContentpurchase_order_defs_finareas_id > .jqx-combobox-input', 'NA')

    }

    verifyFinancialCentreAssertion() {

        cy.valueAreaInput('#dropdownlistContentpurchase_order_defs_finareas_id > .jqx-combobox-input', 'clearing')

    }

    verifyPurchaseOrderFinancialCentersAssertion(FinancialCenters) {

        return cy.getFinancialCenterAssertion('#dropdownlistContentpurchase_order_defs_finareas_id > .jqx-combobox-input', FinancialCenters, 'PurchaseOrder FinancialCenters:')

    }

    verifyDeliveryTo() {

        cy.comboBoxRemoteStrict('#dropdownlistContentpurchase_order_defs_valshiptolocations_id > .jqx-combobox-input', 'STORE')

    }

    verifyDeliveryToValue() {

        cy.valueAreaInput('#dropdownlistContentpurchase_order_defs_valshiptolocations_id > .jqx-combobox-input', 'STORE')

    }

    verifyDeliveryByNA() {

        cy.listbox('#dropdownlistContentpurchase_order_defs_valdeliveryby_id > .jqx-combobox-input')

    }

    verifyDeliveryBy() {

        cy.comboBoxRemoteStrict('#dropdownlistContentpurchase_order_defs_valdeliveryby_id > .jqx-combobox-input', 'NA')

    }

    verifyDeliveryInCharge() {

        cy.comboBoxStrict('#purchase_order_defs_delivery_in_charge', '1000')

    }

    verifyCurrentDeliveryDate() {

        cy.dateTimeInput('#inputpurchase_order_defs_delivery_date', '14/11/2022')

    }

    verifyPastDeliveryDate() {

        cy.dateTimeInput('#inputpurchase_order_defs_delivery_date', '01/09/2022')

    }

    verifyFutureDeliveryDate() {

        cy.dateTimeInput('#inputpurchase_order_defs_delivery_date', '12/12/2022')

    }

    verifyDeliveryDateValue() {

        cy.valueAreaInput('#inputpurchase_order_defs_delivery_date', '26/10/2021')

    }

    verifyDeliveryAddress() {

        cy.comboBoxStrict('#purchase_order_defs_delivery_address', 'No 65 v v kovil street ,kosapet,chennai 12')

    }

    verifyDeliveryAddressValue() {

        cy.valueAreaInput('#purchase_order_defs_delivery_address', 'No 65 v v kovil street ,kosapet,chennai 12')

    }

    verifyTaxGroup() {

        cy.comboBoxRemoteStrict('#dropdownlistContentpurchase_order_defs_taxgroupdefs_id > .jqx-combobox-input', 'VAT 0%')

    }

    verifyTaxGroupValue() {

        cy.valueAreaInput('#dropdownlistContentpurchase_order_defs_taxgroupdefs_id > .jqx-combobox-input', 'VAT 0%')

    }

    verifyTaxGroupMandatoryFields() {

        cy.textAreaInput('#purchase_order_defs_taxgroupdefs_id_div > .formInputFields > .jqx-validator-error-label', 'TAX GROUP is required')

    }

    verifyItemReceiptBtn() {

        cy.cssElement('#ItemReceipts_id')

    }

    verifyPurchaseInvoiceBtn() {

        cy.cssElement('#PurchaseInvoice_id')

    }

    verifyPOPdfReport() {

        cy.reportPdf(0, '#reportActionBtn > :nth-child(6) > #form_print_templatesBtnGrp0')

    }

    verifyFormEditData() {

        cy.formEditData('#purchase_order_defs_id', 'BIL-000105')

    }

    verifyFormEditDataDown() {

        cy.formEditDataDown('#dropdownlistArrowpurchase_order_defs_id')

    }

    verifyAddItems() {

        cy.gridAddItems('purchase_order_items')

    }

    verifyGetGridCellValueItemName() {

        cy.getGridCellValue('purchase_order_items', 'purchase_order_items_itemmaster_idDisplay', 'PurchaseOrder ItemName')

    }

    verifyGetGridCellValueQty() {

        cy.getGridCellValue('purchase_order_items', 'purchase_order_items_qty', 'PurchaseOrder Qty')

    }

    verifyGridCellTotal() {

        cy.getTotalGridCellValue('PurchaseOrder Total:')

    }

    verifyQtyItems() {

        cy.setGridCellValue('purchase_order_items', 0, 'purchase_order_items_qty', '10');

    }

    verifyDeleteDetails() {

        cy.gridDelete('purchase_order_items', [6, 5])

    }

    verifyDeleteAllRows() {

        cy.gridDeleteAllRows('purchase_order_items')

    }

    verifyAutoResizeColumns() {

        cy.gridAutoResize('purchase_order_items')

    }

    verifyQuickAddwithCode() {

        cy.gridQuickAdd('purchase_order_items', '0002')

    }

    verifyQuickAddwithName() {

        cy.gridQuickAdd('purchase_order_items', 'Fabircation')

    }

    verifyCopyTableDataintoClipboard() {

        cy.gridCopyTableData('purchase_order_items')

    }

    verifyPasteFromExcel() {

        cy.gridPasteFromExcel('purchase_order_items')

    }

}

export default new POPage();