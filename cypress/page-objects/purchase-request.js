import { config } from '../env/pr-datas'

class PRPage {

    visit() {

        cy.visitUrlWithQueryParam('/admin/forms/loadForm/159', '?openInTab=current')

    }

    visitReport() {

        cy.visit("/admin/reports/runReport/39")
        cy.wait(8000)

    }

    verifySalesOrderClose() {

        cy.get('#purchaseintent_salesorder_salesorderdefs_id').type('{backspace}')

    }

    verifySalesOrderNA() {

        cy.input('#purchaseintent_salesorder_salesorderdefs_id')

    }

    verifySingleSalesOrder() {

        cy.comboBoxMultiSelectArray('#purchaseintent_salesorder_salesorderdefs_id', ['SO-00273'])

    }

    
    verifyMultiSalesOrder() {

        cy.comboBoxMultiSelectArray('#purchaseintent_salesorder_salesorderdefs_id', ['SO-00273', 'SO-00272', 'SO-00271', 'SO-00270'])

    }

    verifySalesOrderText() {

        cy.textAreaInput('#purchaseintent_salesorder_salesorderdefs_id', config.na)

    }

    verifySalesOrderMandatoryFields() {

        cy.textAreaInput('#purchaseintent_salesorder_salesorderdefs_id_div > .formInputFields > .jqx-validator-error-label', config.salesOrderRequired);

    }

    verifyProject() {

        cy.comboBoxStrict('#purchase_intent_defs_project', config.project)

    }

    verifyProjectAssertion() {

        cy.valueAreaInput('#purchase_intent_defs_project', config.project)

    }

    verifyCustomerPO() {

        cy.comboBoxStrict('#purchase_intent_defs_customer_po', config.customerPO)

    }

    verifyCustomerPOAssertion() {

        cy.valueAreaInput('#purchase_intent_defs_customer_po', config.customerPO)

    }

    verifyCustomerLocation() {

        cy.comboBoxStrict('#purchase_intent_defs_customer_location', config.customerLocation)

    }

    verifyCustomerLocationAssertion() {

        cy.valueAreaInput('#purchase_intent_defs_customer_location', config.customerLocation)

    }

    verifyPurchaseRequestRequestedBy() {

        return cy.getStoreVariable('#dropdownlistContentpurchase_intent_defs_employeemaster_id > .jqx-combobox-input')

    }

    verifyRequestedBy() {

        cy.comboBoxRemoteStrict('#dropdownlistContentpurchase_intent_defs_employeemaster_id > .jqx-combobox-input', config.requestedBy)

    }

    verifyRequestedByAssertion() {

        cy.valueAreaInput('#dropdownlistContentpurchase_intent_defs_employeemaster_id > .jqx-combobox-input', config.requestedByValue)

    }

    verifyAssignedTo() {

        cy.comboBoxRemoteStrict('#dropdownlistContentpurchase_intent_defs_assigned_employeemaster_id > .jqx-combobox-input', config.assignedTo)

    }

    verifyAssignedToAssertion() {

        cy.valueAreaInput('#dropdownlistContentpurchase_intent_defs_assigned_employeemaster_id > .jqx-combobox-input', config.assignedToValue)

    }

    verifyCurrentDate() {

        cy.dateTimeInput('#inputpurchase_intent_defs_date', config.currentDate)

    }

    verifyPastDate() {

        cy.dateTimeInput('#inputpurchase_intent_defs_date', config.pastDate)

    }

    verifyFutureDate() {

        cy.dateTimeInput('#inputpurchase_intent_defs_date', config.futureDate)

    }

    verifyReason() {

        cy.comboBoxStrict('#purchase_intent_defs_reason', config.reason)

    }

    verifyReasonText() {

        cy.textAreaInput('#purchase_intent_defs_reason', config.na)

    }

    verifyMemo() {

        cy.comboBoxStrict('#purchase_intent_defs_memo', config.memo)

    }

    verifyMemoAssertion() {

        cy.valueAreaInput('#purchase_intent_defs_memo', config.memo)

    }

    verifyStatusOpenOrClose() {

        cy.get('#OPEN_OR_CLOSE_PR').should('be.enabled').click({ force: true })

    }

    verifyStatusText() {

        cy.valueAreaInput('#dropdownlistContentpurchase_intent_defs_status > .jqx-combobox-input', config.status)

    }

    verifyApprovedNo() {

        cy.comboBoxRemoteStrict('#purchase_intent_defs_approved', config.approvedNo)

    }

    verifyApprovedYes() {

        cy.comboBoxRemoteStrict('#purchase_intent_defs_approved', config.approvedYes)

    }

    verifyApprovedReject() {

        cy.comboBoxRemoteStrict('#purchase_intent_defs_approved', config.approvedReject)

    }

    verifyPurchaseRequestCustomerJob() {

        return cy.getStoreVariable('#dropdownlistContentpurchase_intent_defs_customer_id > .jqx-combobox-input')

    }

    verifyCustomerJob() {

        cy.comboBoxRemoteStrict('#dropdownlistContentpurchase_intent_defs_customer_id > .jqx-combobox-input', config.customerJob)

    }

    verifyCustomerJobMandatoryFields() {

        cy.textAreaInput('#purchase_intent_defs_customer_id_div > .formInputFields > .jqx-validator-error-label', config.customerJobRequired);

    }

    verifyPurchaseRequestFinancialCenters() {

        return cy.getStoreVariable('#dropdownlistContentpurchase_intent_defs_finareas_id > .jqx-combobox-input')

    }

    verifyFinancialCenters() {

        cy.comboBoxRemoteStrict('#dropdownlistContentpurchase_intent_defs_finareas_id > .jqx-combobox-input', config.financialCenters)

    }

    verifyFinancialCentersMandatoryFields() {

        cy.textAreaInput('#purchase_intent_defs_finareas_id_div > .formInputFields > .jqx-validator-error-label', config.financialCentersRequired);

    }

    verifyForLocation() {

        cy.comboBoxRemoteStrict('#purchase_intent_defs_vallocations_id', config.forLocation)

    }

    verifyForLocationAssertion() {

        cy.valueAreaInput('#dropdownlistContentpurchase_intent_defs_vallocations_id', config.forLocation)

    }

    verifyAddItems() {

        cy.gridAddItems('purchase_intent_items')

    }

    verifyGridCellValueItemName() {

        return cy.getGridCellValue('purchase_intent_items', 'purchase_intent_items_itemmaster_idDisplay');

    }

    verifyGridCellValueQty() {

        return cy.getGridCellValue('purchase_intent_items', 'purchase_intent_items_qty');

    }

    verifyQtyNeededItems() {

        cy.setGridCellValue('purchase_intent_items', 0, 'purchase_intent_items_qty', '10');

    }

    verifyPriceItems() {

        cy.setGridCellValue('purchase_intent_items', 0, 'purchase_intent_items_price', '11.88');

    }

    verifyMemoItems() {

        cy.setGridCellValue('purchase_intent_items', 2, 'purchase_intent_items_memo', 'Customer');

    }

    verifyUnitItems() {

        cy.setGridCellDropdownValue('purchase_intent_items', 0, 'purchase_intent_items_valuom_id', 'BTL');

    }

    verifyCustomerJobItems() {

        cy.setGridCellDropdownValue('purchase_intent_items', 1, 'purchase_intent_items_customer_id', 'C-1454 - TESTING ');

    }

    verifyFinCentersItems() {

        cy.setGridCellDropdownValue('purchase_intent_items', 1, 'purchase_intent_items_finareas_id', 'cleaning');

    }

    verifyGetGridCellValue() {

        cy.getGridCellValue('purchase_intent_items', 1, 'CODELabel')

    }

    verifyDeleteDetails() {

        cy.gridDelete('purchase_intent_items', 0)

    }

    verifyDeleteAllRows() {

        cy.gridDeleteAllRows('purchase_intent_items')

    }

    verifyAutoResizeColumns() {

        cy.gridAutoResize('purchase_intent_items')

    }

    verifyCopyTableDataintoClipboard() {

        cy.gridCopyTableData('purchase_intent_items')

    }

    verifyPasteFromExcel() {

        cy.gridPasteFromExcel('purchase_intent_items')

    }

    verifyQuickAddwithCode() {

        cy.gridQuickAdd('purchase_intent_items', '0002')

    }

    verifyQuickAddwithName() {

        cy.gridQuickAdd('purchase_intent_items', 'Fabircation')

    }

    verifyFormEditData() {

        cy.get('#a2Label')
            .click({ force: true })
            .wait(8000)
        cy.get('#dropdownlistArrowpurchase_intent_defs_id').click();

        cy.get('body').type('{downarrow}');

        cy.get('body').type('{enter}');

        cy.wait(8000)

    }

    verifyFormPrint() {

        cy.formPrint('#printList_PURCHASE_INTENT')

    }

    verifyPRPdfReport() {

        cy.reportPdf(0, '#reportActionBtn > :nth-child(5) > #form_print_templatesBtnGrp0')

    }

    verifyDropFiles() {

        cy.fileInput('#DROPFILESTOUPLOADLegend > .material-symbols-rounded', '#purchase_intent_defs_documentmanager_id_file', 'TestCases.xlsx')

    }

    verifyMultiDropFiles() {

        cy.multiFileInput('#DROPFILESTOUPLOADLegend > .material-symbols-rounded', '#purchase_intent_defs_documentmanager_id_file', ['TestCases.xlsx', 'Testing_Docs.docx']);

    }


}

export default new PRPage();
