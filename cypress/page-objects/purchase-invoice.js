
class PIPage {

    visit() {

        cy.visitUrlWithQueryParam('/admin/forms/loadForm/315', '?openInTab=current')

    }

    visitReport() {

        cy.visit("/admin/reports/runReport/335")
        cy.wait(8000)

    }

    urlReport() {

        cy.url().should('contain', '/admin/reports/runReport/335')

    }

    urlCopy() {

        cy.url().should('contain', '?copy=true')

    }

    verifyPOAssertion() {

        cy.displayedText('#dropdownlistContentpurchaseinvoice_purchaseorder_purchaseorderdefs_id', 'Displayed value in PurchaseInvoice PurchaseOrder Field:')

    }

    verifyPurchaseInvoiceSupplier() {

        return cy.getStoreVariable('#dropdownlistContentpurchase_invoice_defs_supplier_id > .jqx-combobox-input')

    }

    verifySupplier() {

        cy.comboBoxRemoteStrict('#dropdownlistContentpurchase_invoice_defs_supplier_id > .jqx-combobox-input', 'V-0001 - Supplier 1')

    }

    verifySupplierAssertion() {

        cy.valueAreaInput('#dropdownlistContentpurchase_invoice_defs_supplier_id > .jqx-combobox-input', 'V-0001 - Supplier 1')

    }

    verifyPurchaseInvoiceSupplierAssertion(Supplier) {

        return cy.getSupplierAssertion('#dropdownlistContentpurchase_invoice_defs_supplier_id > .jqx-combobox-input', Supplier, 'PurchaseInvoice Supplier:')

    }

    verifyApproved() {

        cy.comboBoxRemoteStrict('#dropdownlistContentpurchase_invoice_defs_approved > .jqx-combobox-input', 'YES')

    }

    verifyPoNA() {

        cy.comboBox('#purchaseinvoice_purchaseorder_purchaseorderdefs_id', 'NA')

    }

    verifySinglePo() {

        cy.comboBoxMultiSelectValue('#dropdownlistContentpurchaseinvoice_purchaseorder_purchaseorderdefs_id > .jqx-combobox-input', ['PO-000476'])

    }

    verifyMultiPo() {

        cy.comboBoxMultiSelectValue('#dropdownlistContentpurchaseinvoice_purchaseorder_purchaseorderdefs_id > .jqx-combobox-input', ['PO-000418', 'PO-000419'])

    }

    verifySupplierRef() {

        cy.comboBoxStrict('#purchase_invoice_defs_supplier_ref', 'purchase order')

    }

    verifyCurrentDate() {

        cy.dateTimeInput('#purchase_invoice_defs_date', '07/06/2023')

    }

    verifyPastDate() {

        cy.dateTimeInput('#purchase_invoice_defs_date', '21/04/2021')

    }

    verifyFutureDate() {

        cy.dateTimeInput('#purchase_invoice_defs_date', '12/12/2024')

    }

    verifyDateValue() {

        cy.valueAreaInput('#purchase_invoice_defs_date', '13/01/2023')

    }

    verifyMemo() {

        cy.comboBoxStrict('#purchase_invoice_defs_memo', 'ENTER DETAILS')

    }

    verifyCurrentDueDate() {

        cy.dateTimeInput('#purchase_invoice_defs_due_date', '07/06/2023')

    }

    verifyPastDueDate() {

        cy.dateTimeInput('#purchase_invoice_defs_due_date', '21/04/2021')

    }

    verifyFutureDueDate() {

        cy.dateTimeInput('#purchase_invoice_defs_due_date', '12/12/2024')

    }

    verifyCustomerJob() {

        cy.comboBoxRemoteStrict('#dropdownlistContentpurchase_invoice_defs_customer_id > .jqx-combobox-input', 'NA NA NA')

    }

    verifyCustomerJobAssertion() {

        cy.valueAreaInput('#dropdownlistContentpurchase_invoice_defs_customer_id > .jqx-combobox-input', 'C-1503 - QA Sales receipts')

    }

    verifyPurchaseInvoiceCustomerJobAssertion(CustomerJob) {

        return cy.getCustomerJobAssertion('#dropdownlistContentpurchase_invoice_defs_customer_id > .jqx-combobox-input', CustomerJob, 'PurchaseInvoice CustomerJob:')

    }

    verifyFinancialCenters() {

        cy.comboBoxRemoteStrict('#dropdownlistContentpurchase_invoice_defs_finareas_id > .jqx-combobox-input', 'Mining Division')

    }

    verifyFinancialCentersValue() {

        cy.valueAreaInput('#dropdownlistContentpurchase_invoice_defs_finareas_id > .jqx-combobox-input', '')

    }

    verifyTaxGroup() {

        cy.comboBoxRemoteStrict('#dropdownlistContentpurchase_invoice_defs_taxgroupdefs_id > .jqx-combobox-input', 'NA')

    }

    verifyTaxGroupValue() {

        cy.valueAreaInput('#dropdownlistContentpurchase_invoice_defs_taxgroupdefs_id > .jqx-combobox-input', 'VAT EXC - 15%')

    }

    verifyAddStock() {

        cy.comboBoxRemoteStrict('#dropdownlistContentpurchase_invoice_defs_add_stock > .jqx-combobox-input', 'YES')

    }

    verifyAddItems() {

        cy.gridAddItems('purchase_invoice_items')

    }

    verifyTotalTaxValue() {

        cy.valueAreaInput('#purchase_invoice_defs_total_tax > .jqx-input-content', '0.00')

    }

    verifyDeleteDetails() {

        cy.gridDelete('purchase_invoice_items', [4, 3])

    }

    verifyDeleteAllRows() {

        cy.gridDeleteAllRows('purchase_invoice_items')

    }

    verifyAutoResizeColumns() {

        cy.gridAutoResize('purchase_invoice_items')

    }

    verifyQuickAddwithCode() {

        cy.gridQuickAdd('purchase_invoice_items', '0002')

    }

    verifyQuickAddwithName() {

        cy.gridQuickAdd('purchase_invoice_items', 'Fabircation')

    }

    verifyCopyTableDataintoClipboard() {

        cy.gridCopyTableData('purchase_invoice_items')

    }

    verifyGetGridCellValueItemName() {

        cy.getGridCellValue('purchase_invoice_items', 'purchase_invoice_items_itemmaster_idDisplay', 'PurchaseInvoice ItemName')

    }

    verifyGetGridCellValueQty() {

        cy.getGridCellValue('purchase_invoice_items', 'purchase_invoice_items_qty', 'PurchaseInvoice Qty')

    }

    verifyGridCellTotal() {

        cy.getTotalGridCellValue('PurchaseInvoice Total:')

    }

    verifyQtyItems() {

        cy.setGridCellValue('purchase_invoice_items', 0, 'purchase_invoice_items_qty', '5');

    }

    verifyGridCellValueItemName() {

        return cy.getGridCellValue('purchase_invoice_items', 'purchase_invoice_items_itemmaster_idDisplay');

    }

    verifyGridCellValueQty() {

        return cy.getGridCellValue('purchase_invoice_items', 'purchase_invoice_items_qty');

    }

    verifyGridCellValueItemNameAssertion(ItemName) {

        return cy.getGridCellValueAssertion('purchase_invoice_items', 'purchase_invoice_items_itemmaster_idDisplay', ItemName, 'PurchaseInvoice ItemName:')

    }

    verifyGridCellValueQtyAssertion(Qty) {

        return cy.getGridCellValueAssertion('purchase_invoice_items', 'purchase_invoice_items_qty', Qty, 'PurchaseInvoice Qty:')

    }

    verifyFormEditData() {

        cy.formEditData('#dropdownlistContentpurchase_invoice_defs_id > .jqx-combobox-input', 'PIV-00353')

    }

    verifyFormEditDataDown() {

        cy.formEditDataDown('#dropdownlistArrowpurchase_invoice_defs_id')

    }

    verifyFormPrint() {

        cy.formPrint('#printList_PURCHASE_INVOICE')

    }

    verifyPIPdfReport() {

        cy.reportPdf(0, '#reportActionBtn > :nth-child(6) > #form_print_templatesBtnGrp0')

    }

    verifyContains() {

        // cy.contains('Cannot edit already approved Purchase Invoice.')

        // cy.contains('Data saved successfully')

        cy.contains('Data added successfully')

    }

}

export default new PIPage();