class PReturnPage {

    visit() {

        cy.visitUrlWithQueryParam('/admin/forms/loadForm/160', '?openInTab=current')

    }

    visitReport() {

        cy.visit("/admin/reports/runReport/40")
        cy.wait(8000)

    }

    verifyTransactionType() {

        cy.listBoxs('#dropdownlistContentpurchase_returns_defs_valtranstypes_id > .jqx-combobox-input', 'Purchase Invoice')

    }

    verifyTransactionRef() {

        cy.comboBoxRemoteStrict('#dropdownlistContentpurchase_returns_defs_type_defs_id > .jqx-combobox-input', 'PIV-00372')

    }

    verifyMemo() {

        cy.comboBoxStrict('#purchase_returns_defs_memo', 'Purchase Invoice')

    }
   
    verifyAddItems() {

        cy.gridAddItems('purchase_returns_items')

    }

}

export default new PReturnPage();