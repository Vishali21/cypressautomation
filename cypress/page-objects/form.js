class Form {

    verifyAddItemsGrid() {

        cy.gridBulkSelectAddItems(6, 'MF')

    }

    verifyCheckbox() {

        cy.checkBoxItems([0, 1, 2, 3, 4])

    }

    verifyRowForm() {

        cy.rowForm(14, '1')

    }

}

export default new Form();
