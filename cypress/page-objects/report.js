class Report {

    verifyReportGrid() {

        cy.reportFilter(4, 'PRI-00136')             //doc num

    }

    verifyCollapseReport() {

        cy.collapseReport(1)

    }

    verifyCheckBoxReport() {

        cy.checkBoxReport(1)

    }

    verifyViewReport() {

        cy.reportView(0)

    }

    verifyEditReport() {

        cy.reportEdit(0)

    }

    verifyCopyReport() {

        cy.reportCopy(0)

    }

    verifyDeleteReport() {

        cy.reportDelete(0)

    }

    verifyRightClickEdit() {

        cy.reportRightClickEdit(0)

    }

    verifyRightClickDelete() {

        cy.reportRightClickDelete(0)

    }

    verifyRightClickPeek() {

        cy.reportRightClickPeek(0)

    }

    verifyRightClickCopy() {

        cy.reportRightClickCopy(0)

    }

    verifyRightClickGroupBy() {

        cy.reportRightClickGroupBy(0)

    }

    verifyRightClickSelectRow() {

        cy.reportRightClickSelectRow(0)

    }

    verifyRightClickSelectColumn() {

        cy.reportRightClickSelectColumn(0)

    }

    verifyRightClickHideSelectedColumn() {

        cy.reportRightClickHideSelectedColumn(0)

    }

    verifyRightClickReload() {

        cy.reportRightClickReload(0)

    }

    verifyRightClickAutoAdjust() {

        cy.reportRightClickAutoAdjust(0)

    }

    verifyRightClickCopyTable() {

        cy.reportRightClickCopyTable(0)

    }

    verifyRightClickFreeze() {

        cy.reportRightClickFreeze(0)

    }

    verifyCustomReports() {

        cy.reportCustomReports('Report')

    }

    verifyColumnsFilterShow() {

        cy.reportShowColumnsFilter(['Approved', 'Date'])

    }

    verifyColumnsFilterHide() {

        cy.reportHideColumnsFilter(['Created At', 'Created By'])

    }

}

export default new Report();
