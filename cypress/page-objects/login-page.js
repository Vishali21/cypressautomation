class LoginPage {

    get userNameInput() { return cy.get("#username") }

    get passwordInput() { return cy.get("#password") }

    get loginButton() { return cy.get("button[type='submit']") }

    get errorMessage() { return cy.get('.fw-400 > .m-0') }


    visit() {

        return cy.visit('/')

    }

    url() {

        cy.url().should('contain', '/admin/dashboard/home?loginSuccess=1')

    }
}

export default new LoginPage();
