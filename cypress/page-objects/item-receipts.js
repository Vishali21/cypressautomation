
class IRPage {

    visit() {

        cy.visitUrlWithQueryParam('/admin/forms/loadForm/140', '?openInTab=current')

    }

    visitReport() {

        cy.visit("/admin/reports/runReport/20")
        cy.wait(8000)

    }

    urlReport() {

        cy.url().should('contain', '/admin/reports/runReport/20')

    }

    urlCopy() {

        cy.url().should('contain', '?copy=true')

    }

    verifyPurchaseOrder() {

        cy.comboBoxRemoteStrict('#dropdownlistContentitem_receipts_defs_purchaseorderdefs_id > .jqx-combobox-input', 'PO-000463')

    }

    verifyPurchaseOrderAssertion() {


        cy.displayedValue('#dropdownlistContentitem_receipts_defs_purchaseorderdefs_id > .jqx-combobox-input', 'Displayed value in ItemReceipts PurchaseOrder Field:')

    }

    verifySupplier() {

        cy.comboBoxRemoteStrict('#dropdownlistContentitem_receipts_defs_supplier_id > .jqx-combobox-input', 'V-0004 - CASH CUSTOMER')

    }

    verifySupplierAssertion() {

        cy.valueAreaInput('#dropdownlistContentitem_receipts_defs_supplier_id > .jqx-combobox-input', 'V-0001 - Supplier 1')

    }

    verifyItemReceiptsSupplierAssertion(Supplier) {

        return cy.getSupplierAssertion('#dropdownlistContentitem_receipts_defs_supplier_id > .jqx-combobox-input', Supplier, 'ItemReceipts Supplier:')

    }


    verifySupplierMandatoryFields() {

        cy.textAreaInput('#item_receipts_defs_supplier_id_div > .formInputFields > .jqx-validator-error-label', 'SUPPLIER is required')

    }

    verifyCurrentReceiptDate() {

        cy.dateTimeInput('#item_receipts_defs_date', '23/06/2023')

    }

    verifyPastReceiptDate() {

        cy.dateTimeInput('#item_receipts_defs_date', '21/04/2021')

    }

    verifyFutureReceiptDate() {

        cy.dateTimeInput('#item_receipts_defs_date', '12/12/2024')

    }

    verifyType() {

        cy.comboBoxRemoteStrict('#dropdownlistContentitem_receipts_defs_type > .jqx-combobox-input', 'CREDIT')

    }

    verifyTypeMandatoryFields() {

        cy.textAreaInput('#item_receipts_defs_type_div > .formInputFields > .jqx-validator-error-label', 'TYPE is required')

    }

    verifyTerms() {

        cy.comboBoxRemoteStrict('#dropdownlistContentitem_receipts_defs_valpaymentterms_id > .jqx-combobox-input', 'PDC 15 Days')

    }

    verifyApprovedValue() {

        cy.valueAreaInput('#dropdownlistContentitem_receipts_defs_approved > .jqx-combobox-input', 'YES')

    }

    verifyCustomerJob() {

        cy.comboBoxRemoteStrict('#dropdownlistContentitem_receipts_defs_customer_id > .jqx-combobox-input', 'C-1503')

    }

    verifyCustomerJobAssertion() {

        cy.valueAreaInput('#dropdownlistContentitem_receipts_defs_customer_id > .jqx-combobox-input', 'C-1503 - QA Sales receipts')

    }

    verifyItemReceiptsCustomerJobAssertion(CustomerJob) {

        return cy.getCustomerJobAssertion('#dropdownlistContentitem_receipts_defs_customer_id > .jqx-combobox-input', CustomerJob, 'ItemReceipts CustomerJob:')

    }

    verifyAccountingAreas() {

        cy.comboBoxRemoteStrict('#dropdownlistContentitem_receipts_defs_finareas_id > .jqx-combobox-input', 'NA')

    }

    verifyShipTo() {

        cy.comboBoxRemoteStrict('#dropdownlistContentitem_receipts_defs_valshiptolocations_id > .jqx-combobox-input', 'SITE')

    }

    verifyCurrentDeliveryDate() {

        cy.dateTimeInput('#inputitem_receipts_defs_delivery_date', '24/06/2023')

    }

    verifyPastDeliveryDate() {

        cy.dateTimeInput('#inputitem_receipts_defs_delivery_date', '21/04/2021')

    }

    verifyFutureDeliveryDate() {

        cy.dateTimeInput('#inputitem_receipts_defs_delivery_date', '12/12/2024')

    }

    verifyDeliveryBy() {

        cy.clickSecondSupplier('#dropdownlistContentitem_receipts_defs_valdeliveryby_id > .jqx-combobox-input', 'SUPPLIER')

    }

    verifyTotalValue() {

        cy.valueAreaInput('#item_receipts_defs_total > .jqx-input-content', '9101.75')

    }

    verifyStatusValue() {

        cy.valueAreaInput('#dropdownlistContentitem_receipts_defs_status > .jqx-combobox-input', 'ACTIVE')

    }

    verifyMemo() {

        cy.comboBoxStrict('#item_receipts_defs_memo', 'purchase order')

    }

    verifyAddItems() {

        cy.gridAddItems('item_receipts_items')

    }

    verifyDeleteDetails() {

        cy.gridDelete('item_receipts_items', [4, 3])

    }

    verifyDeleteAllRows() {

        cy.gridDeleteAllRows('item_receipts_items')

    }

    verifyAutoResizeColumns() {

        cy.gridAutoResize('item_receipts_items')

    }

    verifyQuickAddwithCode() {

        cy.gridQuickAdd('item_receipts_items', '0002')

    }

    verifyQuickAddwithName() {

        cy.gridQuickAdd('item_receipts_items', 'Fabircation')

    }

    verifyCopyTableDataintoClipboard() {

        cy.gridCopyTableData('item_receipts_items')

    }

    verifyGetGridCellValueItemName() {

        cy.getGridCellValue('item_receipts_items', 'item_receipts_items_itemmaster_idDisplay', 'ItemReceipts ItemName')

    }

    verifyGetGridCellValueQty() {

        cy.getGridCellValue('item_receipts_items', 'item_receipts_items_qty', 'ItemReceipts Qty')

    }

    verifyGridCellValueItemNameAssertion(ItemName) {

        return cy.getGridCellValueAssertion('item_receipts_items', 'item_receipts_items_itemmaster_idDisplay', ItemName, 'ItemReceipts ItemName:')

    }

    verifyGridCellValueQtyAssertion(Qty) {

        return cy.getGridCellValueAssertion('item_receipts_items', 'item_receipts_items_qty', Qty, 'ItemReceipts Qty:')

    }

    verifyGridCellValueItemName() {

        return cy.getGridCellValue('item_receipts_items', 'item_receipts_items_itemmaster_idDisplay');

    }

    verifyGridCellValueQty() {

        return cy.getGridCellValue('item_receipts_items', 'item_receipts_items_qty');

    }

    verifyGridCellTotal() {

        cy.getTotalGridCellValue('ItemReceipts Total:')

    }

    verifyQtyItems() {

        cy.setGridCellValue('item_receipts_items', 0, 'item_receipts_items_qty', '5');

    }

    verifyFormEditData() {

        cy.formEditData('#item_receipts_defs_id', 'BIL-000105')

    }

    verifyFormPrint() {

        cy.formPrint('#printList_ITEM_RECEIPT')

    }

    verifyIRPdfReport() {

        cy.reportPdf(0, '#reportActionBtn > :nth-child(6) > #form_print_templatesBtnGrp0')

    }

    verifyContains() {

        // cy.contains('Cannot edit or delete a VOID entry')

        cy.contains('Data saved successfully')

        // cy.contains('Data added successfully')

    }

    verifyLedgerPosting() {

        cy.get('#REPORTSLegend > .material-symbols-rounded').click({ force: true })

    }


}

export default new IRPage();