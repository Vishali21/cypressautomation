class VAPage {

    visit() {

        cy.visitUrlWithQueryParam('/admin/forms/loadForm/154', '?openInTab=current')

    }

    visitReport() {

        cy.visit("/admin/reports/runReport/34")
        cy.wait(8000)

    }

    verifyTransactionType() {

        cy.listBoxs('#dropdownlistContentsupplier_advance_valtranstypes_id > .jqx-combobox-input', 'Purchase Invoice')

    }

    verifyTransactionRef() {

        cy.comboBoxRemoteStrict('#dropdownlistContentsupplier_advance_typedefs_id > .jqx-combobox-input', 'PIV-00003')

    }

    verifyPayFrom() {

        cy.dropDownTreeGrid('#dropDownButtonContentsupplier_advance_finaccounts_id', '.jqx-input-group > .jqx-input', '#row2supplier_advance_finaccounts_idGrid', '1110')

    }

    verifyAmount() {

        cy.numberInput('#supplier_advance_total_without_tax', '2.00')

    }

}

export default new VAPage();