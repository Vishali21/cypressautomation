
Cypress.Commands.add('visitUrlWithQueryParam', (url, queryParam) => {
    const fullUrl = url + queryParam;
    cy.visit(fullUrl).wait(8000)
});


Cypress.Commands.add('cssElement', (selector) => {
    cy.get(selector)
        .click({ force: true })
        .wait(5000)
});


Cypress.Commands.add('xpathElement', (xpath) => {
    cy.xpath(xpath)
        .click({ force: true })
        .wait(5000)
});


Cypress.Commands.add('confirm1', () => {
    return cy.get('#bot1-Msg1')
        .click({ force: true })
        .wait(5000)
});


Cypress.Commands.add('confirm2', () => {
    return cy.get('#bot2-Msg1')
        .click({ force: true })
        .wait(5000)
});


Cypress.Commands.add('comboBoxMultiSelectArray', (id, data) => {
    cy.get(id)
        .type('{backspace}')
    cy.wrap(data).each((value) => {
        cy.get(id)
            .type(value)
            .wait(3000)

        cy.contains(value).click({ force: true })
            .wait(8000);
    })
});


Cypress.Commands.add('comboBoxMultiSelectValue', (id, data) => {
    cy.wrap(data).each((value) => {
        cy.get(id)
            .type(value)
            .wait(3000)

        cy.contains(value).click({ force: true })
            .wait(8000);

    })
});


Cypress.Commands.add('input', (id) => {
    cy.get(id)
        .type('{backspace}')
        .type('NA');

    cy.get('div.jqx-listbox span:contains("NA")')
        .first()
        .click({ force: true })
        .wait(4000);
});

Cypress.Commands.add('clickSecondSupplier', (id, data) => {
    cy.get(id)
        .clear()
        .type(data)
        .wait(3000)

    cy.contains(data)
        .eq(0)
        .click({ force: true })
        .wait(3000)
});


Cypress.Commands.add('comboBoxRemoteStrict', (id, data) => {
    cy.get(id)
        .clear()
        .type(data)
        .wait(3000)

    cy.contains(data)
        .click({ force: true })
        .wait(3000)
});


Cypress.Commands.add('listBoxs', (id, data) => {
    cy.get(id)
        .clear()
        .type(data);

    cy.get(`div.jqx-listbox span:contains(${data})`)
        .first()
        .click({ force: true })
        .wait(4000);
});


Cypress.Commands.add('comboBoxStrict', (id, data) => {
    cy.get(id)
        .clear()
        .type(data)
        .wait(5000)
});


Cypress.Commands.add('dateTimeInput', (id, dateValue) => {
    cy.get(id)
        .invoke('val', dateValue)
        .wait(5000)
});


Cypress.Commands.add('comboBoxMultiSelectRemoteArray', (id, data) => {
    cy.wrap(data).each((value) => {
        cy.get(id)
            .type(value)
            .wait(3000)

        cy.contains(value).click({ force: true })
            .wait(8000);
    })
});


Cypress.Commands.add('comboBox', (id, data) => {
    cy.get(id)
        .type(data);
});


Cypress.Commands.add('timeInput', (id, timeValue) => {
    cy.get(id)
        .invoke('val', timeValue)
        .wait(5000)
});


Cypress.Commands.add('numberInput', (id, data) => {
    cy.get(id)
        .invoke('val', data)
        .wait(5000)
});


Cypress.Commands.add('dropDownTreeGrid', (id1, id2, id3, data) => {
    cy.get(id1)
        .click({ force: true })
        .wait(3000);
    cy.scrollTo(0, 0)
    cy.get(id2)
        .type(`${data}{enter}`)
        .wait(2000);
    cy.scrollTo(0, 0)
    cy.get(id3)
        .dblclick()
});


Cypress.Commands.add('dropDownGrid', (id1, id2, id3, data) => {

    cy.get(id1)
        .click({ force: true })
        .wait(3000);
    cy.get(id2)
        .type(data)
        .wait(2000);
    cy.get(id3)
        .dblclick()
});


Cypress.Commands.add('fileInput', (id1, id2, fileName) => {
    cy.get(id1)
        .click({ force: true })
    cy.wait(5000)
    cy.get(id2)
        .attachFile(fileName, { subjectType: 'drag-n-drop' });
});


Cypress.Commands.add('multiFileInput', (id1, id2, fileNames) => {
    cy.get(id1)
        .click({ force: true })
    cy.wait(5000)
    cy.get(id2)
        .attachFile(fileNames, { subjectType: 'drag-n-drop' });
});


Cypress.Commands.add('fullTextEditorInput', (id, data) => {
    cy.get(id)
        .clear()
        .type(data);
});


Cypress.Commands.add('textEditorInput', (id, data) => {
    cy.get(id)
        .clear()
        .type(data);
});


Cypress.Commands.add('textAreaInput', (id, data) => {
    cy.get(id)
        .should('have.text', data);
});


Cypress.Commands.add('valueAreaInput', (id, value) => {
    cy.get(id)
        .should('have.value', value);
});