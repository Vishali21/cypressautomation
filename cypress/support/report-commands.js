
Cypress.Commands.add('reportFilter', (columnNumber, data) => {
    cy.get('#filterrow\\.reportGrid0')
        .find('.jqx-grid-cell')
        .eq(columnNumber)
        .type(data)
        .wait(8000)
});

Cypress.Commands.add('reportFilterDropdown', (columnNumber, data) => {
    cy.get('#filterrow\\.reportGrid0')
        .find('.jqx-grid-cell')
        .eq(columnNumber)
        .click({ force: true })
        .wait(5000)

    cy.contains(data)
        .click({ force: true })
        .wait(3000)
});

Cypress.Commands.add('reportFilterDate', (columnNumber, dateValue) => {
    cy.get('#filterrow\\.reportGrid0')
        .find('.jqx-grid-cell')
        .eq(columnNumber)
        .wait(3000)
        .invoke('val', dateValue)
        .wait(5000)
});


Cypress.Commands.add('reportView', (rowNumber) => {
    cy.get(`#form_print_templatesBtnGrp${rowNumber}`)
        .find('.material-symbols-rounded')
        .click({ force: true });
    cy.get('#reportActionBtn > :nth-child(1) > #form_print_templatesBtnGrp0')
        .click({ force: true })
        .wait(4000)
});


Cypress.Commands.add('reportEdit', (rowNumber) => {
    cy.get(`#form_print_templatesBtnGrp${rowNumber}`)
        .find('.material-symbols-rounded')
        .click({ force: true });
    cy.get('#reportActionBtn > :nth-child(2) > #form_print_templatesBtnGrp0')
        .click({ force: true })
        .wait(4000)
});


Cypress.Commands.add('reportCopy', (rowNumber) => {
    cy.get(`#form_print_templatesBtnGrp${rowNumber}`)
        .find('.material-symbols-rounded')
        .click({ force: true });
    cy.get('#reportActionBtn > :nth-child(3) > #form_print_templatesBtnGrp0')
        .click({ force: true })
        .wait(4000)
});


Cypress.Commands.add('reportDelete', (rowNumber) => {
    cy.get(`#form_print_templatesBtnGrp${rowNumber}`)
        .find('.material-symbols-rounded')
        .click({ force: true });
    cy.get('#reportActionBtn > :nth-child(4) > #form_print_templatesBtnGrp0')
        .click({ force: true })
        .wait(4000)
});


Cypress.Commands.add('reportPdf', (rowNumber, id) => {
    cy.get(`#form_print_templatesBtnGrp${rowNumber}`)
        .find('.material-symbols-rounded')
        .click({ force: true });
    cy.get(id)
        .click({ force: true })
});


Cypress.Commands.add('collapseReport', (rowNumber) => {
    cy.get(`#row${rowNumber}reportGrid0`)
        .find('.jqx-grid-group-collapse')
        .click({ force: true })
});


Cypress.Commands.add('checkBoxReport', (rowNumber) => {
    cy.get(`#row${rowNumber}reportGrid0`)
        .find('.jqx-checkbox-default')
        .click({ force: true });
});


Cypress.Commands.add('reportCardView', () => {
    return cy.get('#reportBoxViewTab')
        .click({ force: true })
        .wait(5000)
});


Cypress.Commands.add('reportCustomReports', (data) => {
    cy.get('#addUserReportStateBtnDiv')
        .click({ force: true })
        .wait(5000)

    cy.get('#addUserReportState')
        .click({ force: true })
        .wait(5000)

    cy.get('#txt1')
        .type(data)
        .wait(3000)
});


Cypress.Commands.add('reportShowColumnsFilter', (data) => {
    cy.get('#cfilterBtn')
        .click({ force: true })
        .wait(5000)
        .then(() => {
            Cypress._.each(data, (value) => {
                cy.get('#filterreportColumnFilter > .jqx-widget')
                    .clear()
                    .type(value)
                    .wait(3000);

                cy.get('#listitem0reportColumnFilter')
                    .click({ force: true })
                    .wait(3000);
            });
            cy.get('#reportColumnFilterApply')
                .click({ force: true })
                .wait(3000);
        });
});


Cypress.Commands.add('reportHideColumnsFilter', (data) => {
    cy.get('#cfilterBtn')
        .click({ force: true })
        .wait(5000)
        .then(() => {
            Cypress._.each(data, (value) => {
                cy.get('#filterreportColumnFilter > .jqx-widget')
                    .clear()
                    .type(value)
                    .wait(3000);

                cy.get('#listitem0reportColumnFilter')
                    .click({ force: true })
                    .wait(3000);
            });
            cy.get('#reportColumnFilterApply')
                .click({ force: true })
                .wait(3000);
        });
});


Cypress.Commands.add('reportColumnsFilterSelectAll', () => {
    return cy.get('#cfilterBtn')
        .click({ force: true })
        .wait(5000)
        .then(() => {
            cy.get('#reportColumnFilterSelectAll')
                .click({ force: true })
                .wait(3000)

            cy.get('#reportColumnFilterApply')
                .click({ force: true })
                .wait(5000)
        })
});


Cypress.Commands.add('reportClearFilter', () => {
    return cy.get('#clearFilterButton')
        .click({ force: true })
        .wait(5000)
});


Cypress.Commands.add('reportAdjustColumns', () => {
    return cy.get('#autoAdjustBtn')
        .click({ force: true })
        .wait(5000)
});


Cypress.Commands.add('reportReload', () => {
    return cy.get('#reloadBtn')
        .click({ force: true })
        .wait(5000)
});


Cypress.Commands.add('reportCopyTableData', () => {
    return cy.get('#copyGridTable')
        .click({ force: true })
        .wait(5000)
        .then(() => {
            cy.contains('Table Data Copied Successfully')
        });
});


Cypress.Commands.add('reportDownloadEmailExcel', (id) => {
    return cy.get('#downloadAsBtn')
        .click({ force: true })
        .wait(5000)
        .then(() => {
            cy.get('#emailfiles')
                .click({ force: true })
            cy.get('#emailExcel > a')
                .click({ force: true })
        })
});


Cypress.Commands.add('reportDownloadEmailPdf', (id) => {
    return cy.get('#downloadAsBtn')
        .click({ force: true })
        .wait(5000)
        .then(() => {
            cy.get('#emailfiles')
                .click({ force: true })
            cy.get('#emailPdf > a')
                .click({ force: true })
        })
});


Cypress.Commands.add('reportDownloadEmailCsv', (id) => {
    return cy.get('#downloadAsBtn')
        .click({ force: true })
        .wait(5000)
        .then(() => {
            cy.get('#emailfiles')
                .click({ force: true })
            cy.get('#emailCsv > a')
                .click({ force: true })
        })
});


Cypress.Commands.add('reportDownloadExcel', (id) => {
    return cy.get('#downloadAsBtn')
        .click({ force: true })
        .wait(5000)
        .then(() => {
            cy.get('#downloadExcel > a')
                .click({ force: true })
        })
});


Cypress.Commands.add('reportDownloadPdf', (id) => {
    return cy.get('#downloadAsBtn')
        .click({ force: true })
        .wait(5000)
        .then(() => {
            cy.get('#downloadPdf > a')
                .click({ force: true })
        })
});


Cypress.Commands.add('reportDownloadHtml', (id) => {
    return cy.get('#downloadAsBtn')
        .click({ force: true })
        .wait(5000)
        .then(() => {
            cy.get('#downloadHtml > a')
                .click({ force: true })
        })
});


Cypress.Commands.add('reportDownloadCsv', (id) => {
    return cy.get('#downloadAsBtn')
        .click({ force: true })
        .wait(5000)
        .then(() => {
            cy.get('#downloadCsv > a')
                .click({ force: true })
        })
});


Cypress.Commands.add('reportRightClickEdit', (rowNumber) => {
    cy.get(`#form_print_templatesBtnGrp${rowNumber}`)
        .rightclick({ force: true })
        .wait(5000)
    cy.get('#reportGrid0_Hal-context-menu > .reportGridMenu > #hal-context-edit')
        .click({ force: true })
});


Cypress.Commands.add('reportRightClickDelete', (rowNumber) => {
    cy.get(`#form_print_templatesBtnGrp${rowNumber}`)
        .rightclick({ force: true })
        .wait(5000)
    cy.get('#reportGrid0_Hal-context-menu > .reportGridMenu > #hal-context-delete')
        .click({ force: true })
});


Cypress.Commands.add('reportRightClickPeek', (rowNumber) => {
    cy.get(`#form_print_templatesBtnGrp${rowNumber}`)
        .rightclick({ force: true })
        .wait(5000)
    cy.get('#reportGrid0_Hal-context-menu > .reportGridMenu > #hal-context-peek')
        .click({ force: true })
});


Cypress.Commands.add('reportRightClickCopy', (rowNumber) => {
    cy.get(`#form_print_templatesBtnGrp${rowNumber}`)
        .rightclick({ force: true })
        .wait(5000)
    cy.get('#reportGrid0_Hal-context-menu > .reportGridMenu > #hal-context-copy')
        .click({ force: true })
});


Cypress.Commands.add('reportRightClickGroupBy', (rowNumber) => {
    cy.get(`#form_print_templatesBtnGrp${rowNumber}`)
        .rightclick({ force: true })
        .wait(5000)
    cy.get('#reportGrid0_Hal-context-menu > .reportGridMenu > #hal-context-groupby')
        .click({ force: true })
});


Cypress.Commands.add('reportRightClickSelectRow', (rowNumber) => {
    cy.get(`#form_print_templatesBtnGrp${rowNumber}`)
        .rightclick({ force: true })
        .wait(5000)
    cy.get('#reportGrid0_Hal-context-menu > .reportGridMenu > #hal-context-selectrow')
        .click({ force: true })
});


Cypress.Commands.add('reportRightClickSelectColumn', (rowNumber) => {
    cy.get(`#form_print_templatesBtnGrp${rowNumber}`)
        .rightclick({ force: true })
        .wait(5000)
    cy.get('#reportGrid0_Hal-context-menu > .reportGridMenu > #hal-context-selectcolumn')
        .click({ force: true })
});


Cypress.Commands.add('reportRightClickHideSelectedColumn', (rowNumber) => {
    cy.get(`#form_print_templatesBtnGrp${rowNumber}`)
        .rightclick({ force: true })
        .wait(5000)
    cy.get('#reportGrid0_Hal-context-menu > .reportGridMenu > #hal-context-hideselectedcolumn')
        .click({ force: true })
});


Cypress.Commands.add('reportRightClickReload', (rowNumber) => {
    cy.get(`#form_print_templatesBtnGrp${rowNumber}`)
        .rightclick({ force: true })
        .wait(5000)
    cy.get('#reportGrid0_Hal-context-menu > .reportGridMenu > #hal-context-reload')
        .click({ force: true })
});


Cypress.Commands.add('reportRightClickAutoAdjust', (rowNumber) => {
    cy.get(`#form_print_templatesBtnGrp${rowNumber}`)
        .rightclick({ force: true })
        .wait(5000)
    cy.get('#reportGrid0_Hal-context-menu > .reportGridMenu > #hal-context-autoadjust')
        .click({ force: true })
});


Cypress.Commands.add('reportRightClickCopyTable', (rowNumber) => {
    cy.get(`#form_print_templatesBtnGrp${rowNumber}`)
        .rightclick({ force: true })
        .wait(5000)
    cy.get('#reportGrid0_Hal-context-menu > .reportGridMenu > #hal-context-copytable')
        .click({ force: true })
});


Cypress.Commands.add('reportRightClickFreeze', (rowNumber) => {
    cy.get(`#form_print_templatesBtnGrp${rowNumber}`)
        .rightclick({ force: true })
        .wait(5000)
    cy.get('#reportGrid0_Hal-context-menu > .reportGridMenu > #hal-context-freeze')
        .click({ force: true })
});
