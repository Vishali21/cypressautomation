Cypress.Commands.add('formPurchaseOrderBtn', () => {
    return cy.get('#PurchaseOrder_id')
        .click({ force: true })
        .wait(10000)
});

Cypress.Commands.add('formPurchaseInvoiceBtn', () => {
    return cy.get('#PurchaseInvoice_id')
        .click({ force: true })
        .wait(10000)
});

Cypress.Commands.add('formItemReceiptsBtn', () => {
    return cy.get('#ItemReceipts_id')
        .click({ force: true })
        .wait(10000)
});

Cypress.Commands.add('formMakePaymentBtn', () => {
    return cy.get('#PayBills_id')
        .click({ force: true })
        .wait(10000)
});


Cypress.Commands.add('gridBulkSelectAddItems', (columnNumber, data) => {
    cy.get('#filterrow\\.bulkSelectGrid0')
        .find('.jqx-widget')
        .eq(columnNumber)
        .type(data)
        .wait(3000)
});


Cypress.Commands.add('checkBoxItems', (rowNumbers) => {
    rowNumbers.forEach((rowNumber) => {
        cy.get(`#row${rowNumber}bulkSelectGrid0 `)
            .find('.jqx-checkbox-default')
            .click({ force: true });
    });
});


Cypress.Commands.add('gridAddClose', () => {
    return cy.get('#modalAddCloseBulkSelect0')
        .click({ force: true })
        .wait(5000)
});


Cypress.Commands.add('gridAddItemsSelect', () => {
    return cy.get('#modalAddBulkSelect0')
        .click({ force: true })
        .wait(5000)
});


Cypress.Commands.add('rowForm', (columnNumber, data) => {
    cy.get('#row00formGrid0')
        .find('.jqx-widget')
        .eq(columnNumber)
        .type(data)
        .wait(3000)
});


Cypress.Commands.add('setGridCellValue', (gridTableName, rowIndex, columnId, data) => {
    cy.window().then((win) => {
        const gridId = win.gc(gridTableName).gridId;
        win.gi(gridId).jqxGrid('setcellvalue', rowIndex, columnId, data);
    });
});


Cypress.Commands.add('setGridCellDropdownValue', (gridTableName, rowIndex, columnId, data) => {
    cy.window().then((win) => {
        const gridId = win.gc(gridTableName).gridId;
        win.gi(gridId).jqxGrid('begincelledit', rowIndex, columnId, true);
    });

    cy.window().then((win) => {
        const gridId = win.gc(gridTableName).gridId;
        cy.get(`#comboboxeditor${gridId}${columnId}`).invoke('val', data);
    });

    cy.window().then((win) => {
        const gridId = win.gc(gridTableName).gridId;
        win.gi(gridId).jqxGrid('endcelledit', rowIndex, columnId, false);
    });
});


Cypress.Commands.add('getGridCellValue', (gridTableName, columnId) => {
    return cy.window().then((win) => {
        const gridId = win.gc(gridTableName).gridId;
        const rowCount = win.gi(gridId).jqxGrid('getdatainformation').rowscount;
        const cellValues = [];

        for (let i = 0; i < rowCount; i++) {
            const cellValue = win.gi(gridId).jqxGrid('getcellvalue', i, columnId);
            cellValues.push({ rowIndex: i, value: cellValue });
        }

        return cy.wrap(cellValues);
    });
});


Cypress.Commands.add('getGridCellValueAssertion', (gridTableName, columnId, page1CellValues, naming) => {
    return cy.getGridCellValue(gridTableName, columnId).then((cellValuesPage2) => {
        // Compare the cell values of page 1 with page 2
        page1CellValues.forEach((cell, index) => {
            const expectedValue = `${cell.rowIndex}: ${cell.value}`;
            const page2CellValue = `${cellValuesPage2[index].rowIndex}: ${cellValuesPage2[index].value}`;

            expect(page2CellValue).to.equal(expectedValue);

            // Add the expected value to the test context for reporting
            cy.addTestContext(`${naming} ${expectedValue}`);
        });
        return cy.wrap(cellValuesPage2);
    });
});


Cypress.Commands.add('getTotalGridCellValue', (naming) => {
    return cy.get('#statusrowformGrid0 > .jqx-grid-cell')
        .invoke('text')
        .then((text) => {
            cy.log('Total in formGrid:', text);
            expect(text).to.equal(text);
            cy.wrap(text).then((text) => {
                console.log(text);
                cy.addTestContext(`${naming} ${text}`);
            });
            return cy.wrap(text);
        });
});


Cypress.Commands.add('displayedValue', (id, naming) => {
    return cy.get(id).invoke('val').then((value) => {
        cy.log('Displayed value in the dropdown: ' + value);
        expect(value).to.equal(value); // Fix the expect statement here
        cy.wrap(value).then((value) => {
            console.log(value);
            cy.addTestContext(`${naming} ${value}`);
        });
        return cy.wrap(value);
    });
});


Cypress.Commands.add('displayedText', (id, naming) => {
    return cy.get(id).invoke('text').then((text) => {
        cy.log('Displayed value in the dropdown: ' + text);
        expect(text).to.equal(text); // Fix the expect statement here
        cy.wrap(text).then((text) => {
            console.log(text);
            cy.addTestContext(`${naming} ${text}`);
        });
        return cy.wrap(text);
    });
});


Cypress.Commands.add('getStoreVariable', (id) => {
    return cy.get(id)
        .invoke('val')
});


Cypress.Commands.add('getRequestedByAssertion', (id, RequestedBy, naming) => {
    return cy.get(id)
        .should(($element) => {
            const actualValue = $element.val();
            if (actualValue !== RequestedBy) {
                throw new Error(`"${RequestedBy}" is not present in RequestedBy`);
            }
        })
        .then(() => {
            cy.addTestContext(`${naming} ${RequestedBy}`);
        });
});


Cypress.Commands.add('getCustomerJobAssertion', (id, CustomerJob, naming) => {
    return cy.get(id)
        .should(($element) => {
            const actualValue = $element.val();
            if (actualValue !== CustomerJob) {
                throw new Error(`"${CustomerJob}" is not present in CustomerJob`);
            }
        })
        .then(() => {
            cy.addTestContext(`${naming} ${CustomerJob}`);
        });
});

Cypress.Commands.add('getFinancialCenterAssertion', (id, FinancialCenters, naming) => {
    return cy.get(id)
        .should(($element) => {
            const actualValue = $element.val();
            if (actualValue !== FinancialCenters) {
                throw new Error(`"${FinancialCenters}" is not present in FinancialCenters`);
            }
        })
        .then(() => {
            cy.addTestContext(`${naming} ${FinancialCenters}`);
        });
});


Cypress.Commands.add('getSupplierAssertion', (id, Supplier, naming) => {
    return cy.get(id)
        .should(($element) => {
            const actualValue = $element.val();
            if (actualValue !== Supplier) {
                throw new Error(`"${Supplier}" is not present Supplier`);
            }
        })
        .then(() => {
            cy.addTestContext(`${naming} ${Supplier}`);
        });
});


// Cypress.Commands.add('formAdd', () => {
//     return cy.get('#FormAdd')
//         .click()
//         .wait(8000)
// });


Cypress.Commands.add('formAdd', () => {
    return cy.get('#FormAdd').then(($button) => {
        if ($button.is(':enabled')) {
            // Button is enabled, proceed with the click action
            cy.wrap($button).click({ force: true }).wait(20000);
        } else {
            throw new Error('"Add button" is disabled');  //error msg if not clickable
        }
    });
});


Cypress.Commands.add('formView', () => {
    return cy.get('#lookupLink')
        .click({ force: true })
        .wait(20000)
});


Cypress.Commands.add('formClear', () => {
    return cy.get('#filterHeaderButtons > #FormClear')
        .click({ force: true })
        .wait(5000)
});


Cypress.Commands.add('formSave', () => {
    return cy.get('#FormSave')
        .click({ force: true })
        .wait(3000)
});


Cypress.Commands.add('formDelete', () => {
    return cy.get('#FormDeleteTop')
        .click({ force: true })
        .wait(5000)
});


Cypress.Commands.add('formCopy', () => {
    return cy.get('#a4Label')
        .click({ force: true })
        .wait(5000)
});


Cypress.Commands.add('formActivityLog', () => {
    return cy.get('#openMyTimelinelog')
        .click({ force: true })
        .wait(3000)
        .then(() => {
            cy.contains('My activity logs')
        });
});


Cypress.Commands.add('formViewTransaction', () => {
    return cy.get('.jarvisheaderbtn')
        .click({ force: true })
        .wait(5000)
});


Cypress.Commands.add('formAddData', () => {
    return cy.get('#a1Label')
        .click({ force: true })
        .wait(5000)
});


Cypress.Commands.add('formEditData', (id, data) => {
    cy.get('#a2Label')
        .click({ force: true })
        .wait(8000)

    cy.get(id)
        .clear()
        .wait(3000)
        .type(data)
        .wait(8000)

    cy.contains(data).click({ force: true });
});


Cypress.Commands.add('formEditDataDown', (id) => {
    cy.get('#a2Label')
        .click({ force: true })
        .wait(5000)

    cy.get(id)
        .click({ force: true })
        .wait(3000)

    cy.get('body')
        .type('{downarrow}')

    cy.get('body')
        .type('{enter}')

    cy.wait(8000)
});


Cypress.Commands.add('downArrow3rd', (id) => {
    cy.get(id)
        .click({ force: true })
        .wait(3000);

    cy.get('body')
        .type('{downarrow}')
        .type('{downarrow}')
        .type('{enter}');

    cy.wait(8000);
});


Cypress.Commands.add('formPrint', (id) => {
    cy.get('#downloadAsBtn')
        .click({ force: true })
        .wait(3000)

    cy.get(id)
        .click({ force: true })
        .wait(3000)
});


Cypress.Commands.add('gridAddItems', (gridTableName) => {
    cy.window().then((win) => {
        const gridId = win.gc(gridTableName).gridId
        // cy.get(`#${gridId}BulkSelectBtn`)
        cy.get(`#${gridId}nodataitem`)
            .click({ force: true })
            .wait(5000);
    });
});


Cypress.Commands.add('gridDelete', (gridTableName, rowIndexArray) => {
    cy.window().then((win) => {
        const gridId = win.gc(gridTableName).gridId;

        cy.wrap(rowIndexArray).each((rowIndex) => {
            cy.get(`#${gridId}`)
                .find(`[role="row"]:eq(${rowIndex})`)
                .click({ force: true })
                .wait(3000);

            cy.get(`#${gridId}DeleteBtn`)
                .click({ force: true })
                .wait(5000);
        });
    });
});


Cypress.Commands.add('gridDeleteAllRows', (gridTableName) => {
    cy.window().then((win) => {
        const gridId = win.gc(gridTableName).gridId
        cy.get(`#${gridId}DeleteAllBtn`)
            .click({ force: true })
            .wait(5000);
    });
});


Cypress.Commands.add('gridAutoResize', (gridTableName,) => {
    cy.window().then((win) => {
        const gridId = win.gc(gridTableName).gridId
        cy.get(`#${gridId}ResizeBtn`)
            .click({ force: true })
            .wait(5000);
    });
});


Cypress.Commands.add('gridQuickAdd', (gridTableName, data) => {
    cy.window().then((win) => {
        const gridId = win.gc(gridTableName).gridId
        cy.get(`#${gridId}fastAddparent`)
            .clear()
            .type(data)
            .wait(5000);
        cy.contains(data).click({ force: true })
    });
});


Cypress.Commands.add('gridCopyTableData', (gridTableName) => {
    cy.window().then((win) => {
        const gridId = win.gc(gridTableName).gridId
        cy.get(`#${gridId}CopyGridTableBtn`)
            .click({ force: true })
            .wait(5000);
        cy.contains('Table Data Copied Successfully')
    });
});


Cypress.Commands.add('gridPasteFromExcel', (gridTableName) => {
    cy.window().then((win) => {
        const gridId = win.gc(gridTableName).gridId
        cy.get(`#${gridId}PasteUploadBtn`)
            .click({ force: true })
            .wait(5000);
    });
});
