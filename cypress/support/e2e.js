// ***********************************************************
// This example support/e2e.js is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

// Import commands.js using ES2015 syntax:
import './commands'
import './form-commands'
import './report-commands'
import 'cypress-mochawesome-reporter/register';
require('cypress-xpath');
import 'cypress-file-upload'
import 'cypress-ag-grid';




import addContext from 'mochawesome/addContext';

Cypress.Commands.add('addTestContext', (context) => {
    cy.once('test:after:run', (test) => {
        addContext({ test }, context);
    });
});


Cypress.on('fail', (err) => {
    console.error(err);
    const options = { weekday: 'short', month: 'short', day: 'numeric', year: 'numeric', hour: 'numeric', minute: 'numeric', hour12: true };
    const timestamp = new Date().toLocaleString('en-IN', options);
    err.message = timestamp + '\n' + err.message;
    throw err;
});



// Alternatively you can use CommonJS syntax:
// require('./commands')