/// <reference types="Cypress" />

import LoginPage from "../page-objects/login-page.js"
import PRPage from "../page-objects/purchase-request.js"
import POPage from "../page-objects/purchase-order.js"
import IRPage from "../page-objects/item-receipts.js"
import PIPage from "../page-objects/purchase-invoice.js"
import VPPage from "../page-objects/vendorbill-payments.js"
import PReturnPage from "../page-objects/purchase-return.js"
import VAPage from "../page-objects/vendor-advance.js"
import Report from "../page-objects/report.js"
import Form from "../page-objects/form.js"
import { config } from '../env/pr-datas.js'


describe('PURCHASE FLOW', () => {
    Cypress.on('uncaught:exception', (err, runnable) => {
        // returning false here prevents Cypress from
        // failing the test
        return false
    })

    let cred;

    before(() => {
        cy.fixture('credentials').then(function (data) {
            cred = data;
        })
    })

    beforeEach(() => {
        LoginPage.visit()

        LoginPage.userNameInput
            .type(cred.user)

        LoginPage.passwordInput
            .type(cred.pass)

        LoginPage.loginButton
            .click()

        LoginPage.url()

    })

    it('TestCase01 - Creating a new Purchase Request to vendor bill payments - by adding Purchase Request  multiple PR', () => {

        // 2 purchase request with purchase order

        PRPage.visit()

        // PRPage.verifySingleSalesOrder(config.salesOrder)

        PRPage.verifyRequestedBy()

        PRPage.verifyAssignedTo()

        PRPage.verifyCustomerJob()

        PRPage.verifyFinancialCenters()

        PRPage.verifyAddItems()

        Form.verifyAddItemsGrid()

        Form.verifyCheckbox()

        cy.gridAddClose()

        cy.formAdd()

        cy.formView()

        PRPage.visit()

        // PRPage.verifySingleSalesOrder(config.salesOrder)

        PRPage.verifyRequestedBy()

        PRPage.verifyPurchaseRequestRequestedBy().then((PurchaseRequestRequestedBy) => {

            PRPage.verifyAssignedTo()

            PRPage.verifyCustomerJob()

            PRPage.verifyPurchaseRequestCustomerJob().then((PurchaseRequestCustomerJob) => {

                PRPage.verifyFinancialCenters()

                PRPage.verifyAddItems()

                Form.verifyAddItemsGrid()

                Form.verifyCheckbox()

                cy.gridAddClose()

                PRPage.verifyGridCellValueItemName().then((PurchaseRequestItemName) => {

                    //     PRPage.verifyGridCellValueQty().then((PurchaseRequestQty) => {

                    cy.formAdd()

                    cy.formView()

                    cy.formPurchaseOrderBtn()

                    POPage.verifyPurchaseRequestDownenter3rd()

                    POPage.verifyPurchaseRequestAssertion()

                    POPage.verifySupplier()

                    POPage.verifyPurchaseOrderSupplier().then((PurchaseOrderSupplier) => {

                        POPage.verifyPurchaseOrderCustomerJobAssertion(PurchaseRequestCustomerJob)

                        POPage.verifyPurchaseOrderCustomerJob().then((PurchaseOrderCustomerJob) => {

                            // POPage.verifyPurchaseOrderRequestedByAssertion(PurchaseRequestRequestedBy)

                            POPage.verifyGridCellValueItemNameAssertion(PurchaseRequestItemName)

                            // POPage.verifyGridCellValueQtyAssertion(PurchaseRequestQty)

                            POPage.verifyGridCellTotal()

                            POPage.verifyQtyItems()

                            POPage.verifyGridCellValueItemName().then((PurchaseOrderItemName) => {

                                POPage.verifyGridCellValueQty().then((PurchaseOrderQty) => {

                                    cy.formAdd()

                                    cy.formView()

                                    // 1st form purchase order to item receipts - purchase invoice

                                    POPage.visit()

                                    POPage.verifyFormEditDataDown()

                                    cy.formItemReceiptsBtn()

                                    IRPage.verifyPurchaseOrderAssertion()

                                    IRPage.verifyItemReceiptsSupplierAssertion(PurchaseOrderSupplier)

                                    IRPage.verifyItemReceiptsCustomerJobAssertion(PurchaseOrderCustomerJob)

                                    IRPage.verifyGridCellValueItemNameAssertion(PurchaseOrderItemName)

                                    IRPage.verifyGridCellValueQtyAssertion(PurchaseOrderQty)

                                    IRPage.verifyGridCellTotal()

                                    IRPage.verifyQtyItems()

                                    IRPage.verifyDeleteDetails()

                                    cy.formAdd()

                                    cy.formView()

                                    cy.go('back')

                                    cy.wait(8000)

                                    POPage.verifyFormEditDataDown()

                                    cy.formPurchaseInvoiceBtn()

                                    PIPage.verifyPurchaseInvoiceSupplierAssertion(PurchaseOrderSupplier)

                                    PIPage.verifyPurchaseInvoiceSupplier().then((PurchaseInvoiceSupplier) => {

                                        PIPage.verifyPOAssertion()

                                        PIPage.verifyApproved()

                                        PIPage.verifyPurchaseInvoiceCustomerJobAssertion(PurchaseOrderCustomerJob)

                                        PIPage.verifyGridCellValueItemNameAssertion(PurchaseOrderItemName)

                                        PIPage.verifyGridCellValueQtyAssertion(PurchaseOrderQty)

                                        PIPage.verifyGridCellTotal()

                                        PIPage.verifyQtyItems()

                                        PIPage.verifyDeleteDetails()

                                        cy.formAdd()

                                        cy.formView()

                                        // 1st form purchase invoice to vendor bill payments

                                        PIPage.visit()

                                        PIPage.verifyFormEditDataDown()

                                        cy.formMakePaymentBtn()

                                        VPPage.verifyVendorBillSupplierAssertion(PurchaseInvoiceSupplier)

                                        VPPage.verifyPayFrom()

                                        VPPage.verifyTotal()

                                        VPPage.verifyGridCellTotal()

                                        cy.formAdd()

                                        cy.formView()

                                        // 2nd form purchase order to item receipts - purchase invoice

                                        POPage.visit()

                                        POPage.verifyFormEditDataDown()

                                        cy.formItemReceiptsBtn()

                                        IRPage.verifyGridCellValueItemName().then((ItemReceiptsItemName) => {

                                            IRPage.verifyGridCellValueQty().then((ItemReceiptsQty) => {

                                                IRPage.verifyPurchaseOrderAssertion()

                                                IRPage.verifyItemReceiptsSupplierAssertion(PurchaseOrderSupplier)

                                                IRPage.verifyItemReceiptsCustomerJobAssertion(PurchaseOrderCustomerJob)

                                                IRPage.verifyGridCellValueItemNameAssertion(ItemReceiptsItemName)

                                                IRPage.verifyGridCellValueQtyAssertion(ItemReceiptsQty)

                                                IRPage.verifyGridCellTotal()

                                                cy.formAdd()

                                                cy.formView()

                                                cy.go('back')

                                                cy.wait(8000)

                                                POPage.verifyFormEditDataDown()

                                                cy.formPurchaseInvoiceBtn()

                                                PIPage.verifyPurchaseInvoiceSupplierAssertion(PurchaseOrderSupplier)

                                                PIPage.verifyPOAssertion()

                                                PIPage.verifyGridCellValueItemName().then((PurchaseInvoiceItemName) => {

                                                    PIPage.verifyGridCellValueQty().then((PurchaseInvoiceQty) => {

                                                        PIPage.verifyApproved()

                                                        PIPage.verifyPurchaseInvoiceCustomerJobAssertion(PurchaseOrderCustomerJob)

                                                        PIPage.verifyGridCellValueItemNameAssertion(PurchaseInvoiceItemName)

                                                        PIPage.verifyGridCellValueQtyAssertion(PurchaseInvoiceQty)

                                                        PIPage.verifyGridCellTotal()

                                                        cy.formAdd()

                                                        cy.formView()

                                                        // 2nd form purchase invoice to vendor bill payments

                                                        PIPage.visit()

                                                        PIPage.verifyFormEditDataDown()

                                                        cy.formMakePaymentBtn()

                                                        VPPage.verifyVendorBillSupplierAssertion(PurchaseInvoiceSupplier)

                                                        VPPage.verifyPayFrom()

                                                        VPPage.verifyTotal()

                                                        VPPage.verifyGridCellTotal()

                                                        cy.formAdd()

                                                        cy.formView()
                                                    })
                                                })
                                            })
                                        })
                                    })
                                })
                            })
                        })
                    })
                })
            })
        })
    })
})
