/// <reference types="Cypress" />

import LoginPage from "../page-objects/login-page.js"
import PRPage from "../page-objects/purchase-request.js"
import POPage from "../page-objects/purchase-order.js"
import IRPage from "../page-objects/item-receipts.js"
import PIPage from "../page-objects/purchase-invoice.js"
import VPPage from "../page-objects/vendorbill-payments.js"
import PReturnPage from "../page-objects/purchase-return.js"
import VAPage from "../page-objects/vendor-advance.js"
import Report from "../page-objects/report.js"
import Form from "../page-objects/form.js"
import { config } from '../env/pr-datas.js'
import '../support/commands.js';


describe('PURCHASE FLOW', () => {
    Cypress.on('uncaught:exception', (err, runnable) => {
        // returning false here prevents Cypress from
        // failing the test
        return false
    })

    let cred;

    before(() => {
        cy.fixture('credentials').then(function (data) {
            cred = data;
        })
    })

    beforeEach(() => {

        LoginPage.visit()

        LoginPage.userNameInput
            .type(cred.user)

        LoginPage.passwordInput
            .type(cred.pass)

        LoginPage.loginButton
            .click()

        LoginPage.url()

    })


    it.only('TestCase01 - Creating a new Purchase Request to vendor bill payments - partial', () => {

        // Purchase request to purchase order

        PRPage.visit()

        // PRPage.verifySingleSalesOrder(config.salesOrder)

        PRPage.verifyRequestedBy()

        PRPage.verifyPurchaseRequestRequestedBy().then((PurchaseRequestRequestedBy) => {

            PRPage.verifyAssignedTo()

            PRPage.verifyCustomerJob()

            PRPage.verifyPurchaseRequestCustomerJob().then((PurchaseRequestCustomerJob) => {

                PRPage.verifyFinancialCenters()

                PRPage.verifyPurchaseRequestFinancialCenters().then((PurchaseRequestFinancialCenters) => {

                    PRPage.verifyAddItems()

                    Form.verifyAddItemsGrid()

                    Form.verifyCheckbox()

                    cy.gridAddClose()

                    PRPage.verifyQtyNeededItems()

                    PRPage.verifyGridCellValueItemName().then((PurchaseRequestItemName) => {

                        PRPage.verifyGridCellValueQty().then((PurchaseRequestQty) => {

                            cy.formAdd()

                            cy.formView()

                            cy.formPurchaseOrderBtn()

                            POPage.verifySupplier()

                            POPage.verifyPurchaseOrderSupplier().then((PurchaseOrderSupplier) => {

                                POPage.verifyPurchaseOrderCustomerJobAssertion(PurchaseRequestCustomerJob)

                                POPage.verifyPurchaseOrderCustomerJob().then((PurchaseOrderCustomerJob) => {

                                    // POPage.verifyPurchaseOrderRequestedByAssertion(PurchaseRequestRequestedBy)

                                    POPage.verifyPurchaseOrderFinancialCentersAssertion(PurchaseRequestFinancialCenters)

                                    POPage.verifyGridCellValueItemNameAssertion(PurchaseRequestItemName)

                                    POPage.verifyGridCellValueQtyAssertion(PurchaseRequestQty)

                                    POPage.verifyGridCellTotal()

                                    POPage.verifyGridCellValueItemName().then((PurchaseOrderItemName) => {

                                        POPage.verifyGridCellValueQty().then((PurchaseOrderQty) => {

                                            cy.formAdd()

                                            cy.formView()

                                            // 1st form purchase order to item receipts - purchase invoice

                                            POPage.visit()

                                            POPage.verifyFormEditDataDown()

                                            cy.formItemReceiptsBtn()

                                            IRPage.verifyPurchaseOrderAssertion()

                                            IRPage.verifyItemReceiptsSupplierAssertion(PurchaseOrderSupplier)

                                            IRPage.verifyItemReceiptsCustomerJobAssertion(PurchaseOrderCustomerJob)

                                            IRPage.verifyGridCellValueItemNameAssertion(PurchaseOrderItemName)

                                            IRPage.verifyGridCellValueQtyAssertion(PurchaseOrderQty)

                                            IRPage.verifyGridCellTotal()

                                            IRPage.verifyQtyItems()

                                            IRPage.verifyDeleteDetails()

                                            cy.formAdd()

                                            cy.formView()

                                            cy.go('back')

                                            cy.wait(8000)

                                            POPage.verifyFormEditDataDown()

                                            cy.formPurchaseInvoiceBtn()

                                            PIPage.verifyPurchaseInvoiceSupplierAssertion(PurchaseOrderSupplier)

                                            PIPage.verifyPurchaseInvoiceSupplier().then((PurchaseInvoiceSupplier) => {

                                                PIPage.verifyPOAssertion()

                                                PIPage.verifyApproved()

                                                PIPage.verifyPurchaseInvoiceCustomerJobAssertion(PurchaseOrderCustomerJob)

                                                PIPage.verifyGridCellValueItemNameAssertion(PurchaseOrderItemName)

                                                PIPage.verifyGridCellValueQtyAssertion(PurchaseOrderQty)

                                                PIPage.verifyGridCellTotal()

                                                PIPage.verifyQtyItems()

                                                PIPage.verifyDeleteDetails()

                                                cy.formAdd()

                                                cy.formView()

                                                // 1st form purchase invoice to vendor bill payments

                                                PIPage.visit()

                                                PIPage.verifyFormEditDataDown()

                                                cy.formMakePaymentBtn()

                                                VPPage.verifyVendorBillSupplierAssertion(PurchaseInvoiceSupplier)

                                                VPPage.verifyPayFrom()

                                                VPPage.verifyTotal()

                                                VPPage.verifyGridCellTotal()

                                                cy.formAdd()

                                                cy.formView()

                                                // 2nd form purchase order to item receipts - purchase invoice

                                                POPage.visit()

                                                POPage.verifyFormEditDataDown()

                                                cy.formItemReceiptsBtn()

                                                IRPage.verifyGridCellValueItemName().then((ItemReceiptsItemName) => {

                                                    IRPage.verifyGridCellValueQty().then((ItemReceiptsQty) => {

                                                        IRPage.verifyPurchaseOrderAssertion()

                                                        IRPage.verifyItemReceiptsSupplierAssertion(PurchaseOrderSupplier)

                                                        IRPage.verifyItemReceiptsCustomerJobAssertion(PurchaseOrderCustomerJob)

                                                        IRPage.verifyGridCellValueItemNameAssertion(ItemReceiptsItemName)

                                                        IRPage.verifyGridCellValueQtyAssertion(ItemReceiptsQty)

                                                        IRPage.verifyGridCellTotal()

                                                        cy.formAdd()

                                                        cy.formView()

                                                        cy.go('back')

                                                        cy.wait(8000)

                                                        POPage.verifyFormEditDataDown()

                                                        cy.formPurchaseInvoiceBtn()

                                                        PIPage.verifyPurchaseInvoiceSupplierAssertion(PurchaseOrderSupplier)

                                                        PIPage.verifyPOAssertion()

                                                        PIPage.verifyGridCellValueItemName().then((PurchaseInvoiceItemName) => {

                                                            PIPage.verifyGridCellValueQty().then((PurchaseInvoiceQty) => {

                                                                PIPage.verifyApproved()

                                                                PIPage.verifyPurchaseInvoiceCustomerJobAssertion(PurchaseOrderCustomerJob)

                                                                PIPage.verifyGridCellValueItemNameAssertion(PurchaseInvoiceItemName)

                                                                PIPage.verifyGridCellValueQtyAssertion(PurchaseInvoiceQty)

                                                                PIPage.verifyGridCellTotal()

                                                                cy.formAdd()

                                                                cy.formView()

                                                                // 2nd form purchase invoice to vendor bill payments

                                                                PIPage.visit()

                                                                PIPage.verifyFormEditDataDown()

                                                                cy.formMakePaymentBtn()

                                                                VPPage.verifyVendorBillSupplierAssertion(PurchaseInvoiceSupplier)

                                                                VPPage.verifyPayFrom()

                                                                VPPage.verifyTotal()

                                                                VPPage.verifyGridCellTotal()

                                                                cy.formAdd()

                                                                cy.formView()

                                                            })
                                                        })
                                                    })
                                                })
                                            })
                                        })
                                    })
                                })
                            })
                        })
                    })
                })
            })
        })
    })

    describe('Create new purchase order with copy purchase order', () => {

        it('TestCase04 - New purchase order with copy purchase order', () => {

            POPage.visit()

            POPage.verifyCopyPurchase()

            cy.formAdd()

            cy.formView()

        })
    })

    describe('Create new IR with/without purchase Order ', () => {

        it('TestCase05 - New item receipts with purchase order', () => {

            IRPage.visit()

            IRPage.verifyPurchaseOrder()

            cy.formAdd()

            cy.formView()

        })

        it('TestCase06 - New item receipts without purchase order', () => {

            IRPage.visit()

            IRPage.verifySupplier()

            IRPage.verifyAddItems()

            Form.verifyAddItemsGrid()

            Form.verifyCheckbox()

            cy.gridAddClose()

            cy.formAdd()

            cy.formView()

        })
    })

    describe('Purchase invoice to vendor bill payments', () => {

        it('TestCase07 - Create new Purchase invoice to Vendor bill payments PI', () => {

            PIPage.visit()

            PIPage.verifySupplier()

            PIPage.verifyPurchaseInvoiceSupplier().then((PurchaseInvoiceSupplier) => {

                PIPage.verifyApproved()

                PIPage.verifyAddItems()

                Form.verifyAddItemsGrid()

                Form.verifyCheckbox()

                cy.gridAddClose()

                cy.formAdd()

                cy.formView()

                cy.formMakePaymentBtn()

                VPPage.verifyVendorBillSupplierAssertion(PurchaseInvoiceSupplier)

                VPPage.verifyPayFrom()

                VPPage.verifyTotal()

                VPPage.verifyGridCellTotal()

                cy.formAdd()

                cy.formView()

            })
        })

        it('TestCase08 - Create new Purchase invoice with Purchase order to Vendor bill payments PI', () => {

            PIPage.visit()

            PIPage.verifySupplier()

            PIPage.verifyPurchaseInvoiceSupplier().then((PurchaseInvoiceSupplier) => {

                PIPage.verifyApproved()

                PIPage.verifySinglePo()

                PIPage.verifyPOAssertion()

                cy.formAdd()

                cy.formView()

                cy.formMakePaymentBtn()

                VPPage.verifyVendorBillSupplierAssertion(PurchaseInvoiceSupplier)

                VPPage.verifyPayFrom()

                VPPage.verifyTotal()

                VPPage.verifyGridCellTotal()

                cy.formAdd()

                cy.formView()

            })
        })

        it('TestCase09 - Create new Purchase invoice with multiple Purchase order to Vendor bill payments PI', () => {

            PIPage.visit()

            PIPage.verifySupplier()

            PIPage.verifyPurchaseInvoiceSupplier().then((PurchaseInvoiceSupplier) => {

                PIPage.verifyApproved()

                PIPage.verifyMultiPo()

                PIPage.verifyPOAssertion()

                cy.formAdd()

                cy.formView()

                cy.formMakePaymentBtn()

                VPPage.verifyVendorBillSupplierAssertion(PurchaseInvoiceSupplier)

                VPPage.verifyPayFrom()

                VPPage.verifyTotal()

                VPPage.verifyGridCellTotal()

                cy.formAdd()

                cy.formView()

            })
        })
    })

    describe('Vendor bill payments by supplier', () => {

        it('TestCase10 - Create new vendor bill Payment by supplier', () => {

            VPPage.visit()

            VPPage.verifyPayTo()

            VPPage.verifyPayFrom()

            VPPage.verifyTotal()

            VPPage.verifyGridCellTotal()

            cy.formAdd()

            cy.formView()

        })
    })

    describe('Purchase return', () => {

        it('TestCase11 - Create new Purchase Return from PI', () => {

            PReturnPage.visit()

            PReturnPage.verifyTransactionType()

            PReturnPage.verifyTransactionRef()

            PReturnPage.verifyMemo()

            cy.formAdd()

            cy.formView()

        })

        it('TestCase12 - Create new Purchase Return from VA', () => {

            PReturnPage.visit()

            PReturnPage.verifyTransactionType()

            PReturnPage.verifyTransactionRef()

            PReturnPage.verifyMemo()

            cy.formAdd()

            cy.formView()

        })

        it('TestCase13 - Create new Purchase Return from IR', () => {

            PReturnPage.visit()

            PReturnPage.verifyTransactionType()

            PReturnPage.verifyTransactionRef()

            PReturnPage.verifyMemo()

            cy.formAdd()

            cy.formView()

        })
    })

    describe('Vendor Advance by Purchase invoice', () => {

        it('TestCase14 - Create new vendor Advance by PI', () => {

            VAPage.visit()

            VAPage.verifyTransactionType()

            VAPage.verifyTransactionRef()

            VAPage.verifyPayFrom()

            VAPage.verifyAmount()

            cy.formAdd()

            cy.formView()

        })
    })

})
