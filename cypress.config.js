const { defineConfig } = require("cypress");


module.exports = defineConfig({


  viewportWidth: 1700,
  viewportHeight: 900,
  // scrollBehavior: "nearest",
  defaultCommandTimeout: 10000,
  pageLoadTimeout: 1000000,
  waitForAnimations: false,
  animationDistanceThreshold: 50,
  chromeWebSecurity: false,
  chromeWebSecurityArgs: "--disable-web-security",


  e2e: {
    setupNodeEvents(on, config) {
      // Set up Mochawesome reporter
      require('cypress-mochawesome-reporter/plugin')(on);

    },

    // Mochawesome html report 
    reporter: "cypress-mochawesome-reporter",
    reporterOptions: {
      "reportDir": "test-results/mochawesome",
      "charts": true,
      "embeddedScreenshots": true,
      "inlineAssets": true,
      "overwrite": false,
      "html": false,
      "json": true,
      "autoOpen": true,
      "reportTitle": "HAL ERP",
      "reportPageTitle": "Cypress Automation Result",
      "code": true,
      "timestamp": false
    },



    // video reporting
    video: true,
    videoUploadOnPasses: false,
    videosFolder: 'test-results/video',


    // screenshots reporting
    screenshotOnRunFailure: true,
    screenshotsFolder: 'test-results/screenshots',


    // base url of the application
    baseUrl: 'https://issamdemo.halerp.com',


    // specPattern: [
    //   'cypress/e2e/itemReceipts.cy.js',
    // ],


  },

});
